'use strict'

Object.defineProperty(exports, '__esModule', {
  value: true
})
Object.defineProperty(exports, 'SelectableGroup', {
  enumerable: true,
  get: function get() {
    return _SelectableGroup.default
  }
})
Object.defineProperty(exports, 'createSelectable', {
  enumerable: true,
  get: function get() {
    return _CreateSelectable.default
  }
})
Object.defineProperty(exports, 'SelectAll', {
  enumerable: true,
  get: function get() {
    return _SelectAll.default
  }
})
Object.defineProperty(exports, 'DeselectAll', {
  enumerable: true,
  get: function get() {
    return _DeselectAll.default
  }
})

var _SelectableGroup = _interopRequireDefault(require('./SelectableGroup'))

var _CreateSelectable = _interopRequireDefault(require('./CreateSelectable'))

var _SelectAll = _interopRequireDefault(require('./SelectAll'))

var _DeselectAll = _interopRequireDefault(require('./DeselectAll'))

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj }
}

// As early as possible, check for the existence of the JavaScript globals which
// Relay Runtime relies upon, and produce a clear message if they do not exist.
if (process.env.NODE_ENV === 'development') {
  if (
    typeof Map !== 'function' ||
    typeof Set !== 'function' ||
    typeof Array.from !== 'function' ||
    typeof Array.isArray !== 'function' ||
    typeof Object.assign !== 'function'
  ) {
    throw new Error(
      '\n      React-Selectable-Fast requires Map, Set, Array.from,\n      Array.isArray, and Object.assign to exist.\n      Use a polyfill to provide these for older browsers.\n    '
    )
  }
}
