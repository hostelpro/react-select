'use strict'

Object.defineProperty(exports, '__esModule', {
  value: true
})
exports.default = void 0

var _react = _interopRequireDefault(require('react'))

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj }
}

var SelectableGroupContext = _react.default.createContext({
  selectable: 'omg'
})

var _default = SelectableGroupContext
exports.default = _default
